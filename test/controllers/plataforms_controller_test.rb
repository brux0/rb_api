require 'test_helper'

class PlataformsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @plataform = plataforms(:one)
  end

  test "should get index" do
    get plataforms_url, as: :json
    assert_response :success
  end

  test "should create plataform" do
    assert_difference('Plataform.count') do
      post plataforms_url, params: { plataform: { description: @plataform.description } }, as: :json
    end

    assert_response 201
  end

  test "should show plataform" do
    get plataform_url(@plataform), as: :json
    assert_response :success
  end

  test "should update plataform" do
    patch plataform_url(@plataform), params: { plataform: { description: @plataform.description } }, as: :json
    assert_response 200
  end

  test "should destroy plataform" do
    assert_difference('Plataform.count', -1) do
      delete plataform_url(@plataform), as: :json
    end

    assert_response 204
  end
end
