class CreateRegisters < ActiveRecord::Migration[6.0]
  def change
    create_table :registers do |t|
      t.integer :pwd
      t.string :description
      t.string :online
      t.float :time
      t.string :comments
      t.date :server

      t.timestamps
    end
  end
end
